import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import Layout from "Components/Layout";

class Home extends Component {
  constructor() {
    super();
    this.state = {
      pokemons: [],
      isLoading: true,
      limit: 10,
      offset: 2,
    };
  }

  componentDidMount() {
    this.fetchdataDenganFetch();
  }

  fetchdataDenganFetch = () => {
    const { limit, offset } = this.state;
    this.setState({ isLoading: true });
    fetch(`https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`)
      .then((response) => response.json())
      .then((data) =>
        this.setState({ pokemons: data.results, isLoading: false })
      )
      .catch((error) => console.log(error));
  };

  render() {
    console.log(this.props);
    const { pokemons, isLoading, offset } = this.state;

    return (
      <Layout>
        <div className="home">
          <div className="home__title">Poke Fav</div>
          <div className="home__grid container">
            {this.props.favorits.length === 0 || isLoading
              ? null
              : this.props.favorits.map((pokemon, index) => {
                  return (
                    <div className="home__grid__item" key={index}>
                      <Link
                        className="home__grid__item__content"
                        to={`/pokemon/${offset + index + 1}`}
                      >
                        <span>{pokemon?.name}</span>
                      </Link>
                    </div>
                  );
                })}
          </div>

          <div className="home__title">Poke List</div>
          <div className="home__grid container">
            {pokemons.length === 0 || isLoading
              ? null
              : pokemons.map((pokemon, index) => {
                  return (
                    <div className="home__grid__item" key={index}>
                      <button
                        className="home__grid__item__save"
                        onClick={() =>
                          this.props.handleAddFavorit({
                            newValue: {
                              id: offset + index + 1,
                              name: pokemon.name,
                            },
                          })
                        }
                      >
                        +
                      </button>

                      <Link
                        className="home__grid__item__content"
                        to={`/pokemon/${offset + index + 1}`}
                      >
                        <span>{pokemon.name}</span>
                      </Link>
                    </div>
                  );
                })}
          </div>
        </div>
      </Layout>
    );
  }
}

//map state to props adalag fungsi untuk me-mapping data dari store-redux ke komponen yang menggunakan (terbaca sebagai props)
const mapStateToProps = (state) => {
  return {
    favorits: state.favoritPokemon,
  };
};

//mapping dispatch as a props
const mapDispatchToProps = (dispatch) => {
  return {
    handleAddFavorit: (params) =>
      dispatch({ type: "SET_FAVORIT_POKEMON", newValue: params.newValue }),
  };
};

//export default connect(mapping-to-props)(component-name)
export default connect(mapStateToProps, mapDispatchToProps)(Home);
