import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import Layout from "Components/Layout";

class Pokedex extends Component {
  render() {
    return (
      <Layout>
        <div className="home">
          <div className="home__title">Poke Fav</div>
          <div className="home__grid container">
            {this.props.favorits.length === 0
              ? null
              : this.props.favorits.map((pokemon, index) => {
                  return (
                    <div className="home__grid__item" key={index}>
                      <Link className="home__grid__item__content">
                        <span>{pokemon?.name}</span>
                      </Link>
                    </div>
                  );
                })}
          </div>
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    favorits: state.favoritPokemon,
  };
};

export default connect(mapStateToProps)(Pokedex);
